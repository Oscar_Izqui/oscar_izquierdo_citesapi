const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const citasData = require("./data-base/cites_db.json");
app.use(express.static('public'));

const app = express();
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static("public"));


// consulta RANDOM les cites
app.get("/api/random", (req, res) => {
    const citaList = citasData.map(el => ({ el }));

    const citaRdm = () => { return Math.floor(Math.random() * 4) }

    res.status(200).json(citaList[citaRdm()]);
});

/* // consulta TOTES les cites
app.get("/api/cites", (req, res) => {
    Cita.find()
        .then(data => {
            // hacemos cosas con los datos antes de enviar...
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

// crea una cita
app.post("/api/cites", (req, res) => {
    if (!req.body.texto) {
        res.status(400).send({ error: "No trobo la cita!" });
        return;
    }

    // cita nova
    const cita = new Cita({
        texto: req.body.texto,
        autor: req.body.autor,
        foto: req.body.foto
    });

    // el guardem a la bdd

    cita.save(cita)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
}); */

// set port, listen for requests
const puerto = process.env.PORT || 8080;
app.listen(puerto, () => {
    console.log(`Server is running on port ${puerto}.`);
});